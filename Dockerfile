FROM eclipse-temurin:17.0.11_9-jre-alpine
MAINTAINER 9494671+paluszkiewiczB@users.noreply.github.com
ENV PLANTUML_VERSION=1.2024.4
ENV LANG en_US.UTF-8
RUN wget "https://github.com/plantuml/plantuml/releases/download/v${PLANTUML_VERSION}/plantuml-${PLANTUML_VERSION}.jar" -O plantuml.jar
RUN apk add --no-cache graphviz fontconfig ttf-dejavu
ENTRYPOINT ["java", "-Djava.awt.headless=true", "-jar", "plantuml.jar", "-p"]
CMD ["-tsvg"]
