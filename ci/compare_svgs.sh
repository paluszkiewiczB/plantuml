#!/bin/bash
# Arg $1 - path to first SVG file
# Arg $1 - path to second SVG file

set -e
set -o pipefail

function hashSvg() {
  #removes trailing comment with md5 hash, then hashes XML part
  head -n 1 "$1" | rev | cut -c 43- | rev | sha256sum
}

first="$1"
second="$2"

first_hash=$(hashSvg "$first")
second_hash=$(hashSvg "$second")

if [ "$first_hash" != "$second_hash" ]; then
  echo "SVG files: $first and $second have different content!"
  exit 1
fi

echo "Hashes are equal for files $first and $second"
