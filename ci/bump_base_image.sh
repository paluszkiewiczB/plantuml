#!/bin/bash

set -ex
set -o pipefail

FROM_LINE_PREFIX="FROM eclipse-temurin:"

current_from_line=$(grep "$FROM_LINE_PREFIX" Dockerfile)
current_tag=${current_from_line#"$FROM_LINE_PREFIX"}

wget -q "https://hub.docker.com/v2/repositories/library/eclipse-temurin/tags?page_size=100" -O - >tmp.json
jq -r ".results[].name"< tmp.json >tags.txt
next=$(jq -r ".next" < tmp.json)
while [[ "$next" != "null" ]]; do
  wget -q "$next" -O - >tmp.json
  jq -r ".results[].name"< tmp.json >>tags.txt
  next=$(jq -r ".next" < tmp.json || "0")
done

last_tag=$(grep jre tags.txt | grep 17 | grep alpine | sort -V | tail -n 1)

if [ "$current_tag" == "$last_tag" ]; then
  echo "Base image tag is up to date, no need to bump"
  exit 0
fi

echo "Found a difference between versions. Current: $current_tag, last: $last_tag"
new_from_line="$FROM_LINE_PREFIX$last_tag"

sed -i 's,'"$current_from_line"','"$new_from_line"',g' Dockerfile

git add Dockerfile
git commit -m "Bump base image version from: ${current_tag} to ${last_tag}"
