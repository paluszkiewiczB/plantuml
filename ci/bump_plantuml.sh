#!/bin/bash

set -e
set -o pipefail

USER="${PLANTUML_GH_USERNAME:-plantuml}"
REPO="${PLANTUML_GH_REPOSITORY:-plantuml}"
VERSION_LINE_PREFIX="ENV PLANTUML_VERSION="

current_version_line=$(grep "$VERSION_LINE_PREFIX" Dockerfile)
current_version=${current_version_line#"$VERSION_LINE_PREFIX"}
echo "current version: ${current_version}"

wget \
  --header "Accept: application/vnd.github+json" \
  "https://api.github.com/repos/${USER}/${REPO}/releases" \
  -O response.json

tags=$(jq -r '.[] | .tag_name | select(test("v\\d+\\.\\d{4}\\.\\d+"))' <response.json)
last_tag=$(echo "$tags" | head -n 1)
last_version=${last_tag#"v"}
echo "last version: ${last_version}"


if [ "$current_version" == "$last_version" ]; then
  echo "PlantUML version is up to date, no need to bump"
  exit 0
fi

echo "Found a difference between versions. Current: $current_version, last: $last_version"
new_version_line="$VERSION_LINE_PREFIX$last_version"

#can potentially downgrade the version if last one will be older (order by release date?)
sed -i 's,'"$current_version_line"','"$new_version_line"',g' Dockerfile

git add Dockerfile
git commit -m "Bump PlantUML version from: ${current_version} to ${last_version}"
