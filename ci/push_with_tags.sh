#!/bin/bash

set -e
set -o pipefail

local_commits_count=$(git rev-list origin/main..HEAD --count)

if [ "$local_commits_count" -eq 0 ]; then
  echo "No local commits, skip pushing and tagging"
  exit 0
fi

FROM_LINE_PREFIX="FROM eclipse-temurin:"
current_from_line=$(grep "$FROM_LINE_PREFIX" Dockerfile)
jre_tag=${current_from_line#"$FROM_LINE_PREFIX"}

VERSION_LINE_PREFIX="ENV PLANTUML_VERSION="
current_version_line=$(grep "$VERSION_LINE_PREFIX" Dockerfile)
platnuml_version=${current_version_line#"$VERSION_LINE_PREFIX"}

tags=("$platnuml_version""_$jre_tag")
echo "Adding tags: ${tags[*]}"

for tag in "${tags[@]}"; do
  git tag "$tag"
done

git push
for tag in "${tags[@]}"; do
  git push origin "refs/tags/$tag:refs/tags/$tag"
done