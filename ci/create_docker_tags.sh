#!/bin/bash
# Arg $1 name of the image (including tag) to create another tags for, e.g. paluszkiewiczb/plantuml:1.2022.6_17.0.3_7-jre-alpine

set -e
set -o pipefail

if [ -z "$1" ]; then
  echo "Required first parameter: name of the image"
  exit 1
fi

input="$1"

image=$(echo "$input" | cut -d ":" -f1)
tag=$(echo "$input" | cut -d ":" -f2)

plantuml_version=$(echo "$tag" | cut -d "_" -f1)

tags=("$image:$plantuml_version" "$image:$plantuml_version-17-jre" "$image:latest")

for tag in "${tags[@]}"; do
  docker tag "$input" "$tag"
  docker push "$tag"
done
