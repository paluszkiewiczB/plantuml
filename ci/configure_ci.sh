#!/bin/sh
# Requires environment variables:
# - GITLAB_CI_TOKEN

set -e

echo "Installing packages"

apk add bash jq git

echo "Configuring git"
git config --global user.email "paluszkiewiczb@noreply.com"
git config --global user.name "[CI] paluszkiewiczb"

url_host=$(git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g")
git remote set-url origin "https://gitlab-ci-token:${GITLAB_CI_TOKEN}@${url_host}"
