[![Docker pulls](https://img.shields.io/docker/pulls/paluszkiewiczb/plantuml.svg)](https://hub.docker.com/r/paluszkiewiczb/plantuml/)
[![Latest Tag](https://img.shields.io/gitlab/v/tag/paluszkiewiczB/plantuml?sort=semver)](https://img.shields.io/gitlab/v/tag/paluszkiewiczB/plantuml?sort=semver/)

# note
It is a clone of: [https://hub.docker.com/r/think/plantuml](https://hub.docker.com/r/think/plantuml)

The difference is - this repository should be automatically updated every week to use the newest version of PlantUML and base JRE image
___

# plantuml

Docker Container for [PlantUML](http://plantuml.com)

## Motivation

To use plantuml, it needs to be downloaded, java needs to be installed, graphviz needs to be installed, and these things need to be chained.
This docker container does this for you. And allows you to pipe by default into plantuml, so it's neat for script usage.

## Usage

```
cat test.uml | docker run --rm -i paluszkiewiczB/plantuml > test.svg
```

The default will output svg. If png output is wanted, call it like this:

```
cat test.uml | docker run --rm -i paluszkiewiczB/plantuml -tpng > test.png
```
